# makefile for my1piloader - a cross-platform Raspberry Pi code loader

PROJECT = my1piloader
CONSBIN = $(PROJECT)
CONSPRO = $(CONSBIN)
CONSOBJ = my1keys.o my1uart.o $(CONSBIN).o
DESTDIR ?= $(HOME)/.local/my1bin
APPVERS = $(shell date +%Y%m%d)

DELETE = rm -rf
COPY = cp -R

CFLAGS += -Wall --static -I../my1codelib/src
LFLAGS +=
OFLAGS +=

ifeq ($(OS),Windows_NT)
DO_MINGW=YES
endif

ifeq ($(DO_MINGW),YES)
	CONSPRO = $(CONSBIN).exe
ifneq ($(OS),Windows_NT)
	# cross-compiler settings
	XTOOL_DIR ?= /home/share/tool/mingw
	XTOOL_TARGET = $(XTOOL_DIR)
	CROSS_COMPILE = $(XTOOL_TARGET)/bin/i686-pc-mingw32-
	# extra switches - do i need these???
	CFLAGS += -I$(XTOOL_DIR)/include
	LFLAGS += -L$(XTOOL_DIR)/lib
endif
	# tell sources this is mingw build
	CFLAGS += -DDO_MINGW
endif

.PHONY: main all new debug strip version install clean

CC = $(CROSS_COMPILE)gcc
CPP = $(CROSS_COMPILE)g++

main: $(CONSPRO)

all: main

new: clean main

debug: CFLAGS += -DMY1DEBUG
debug: new

strip: new
	strip $(CONSBIN)

version: APPVERS = $(shell cat VERSION)
version: new

install: strip
	@if [ ! -d $(DESTDIR) ] ; then \
		echo "** Invalid path "$(DESTDIR)"!" ; \
	else \
		echo -n "-- Installing "$(CONSBIN)" to "$(DESTDIR)"... " ; \
		cp $(CONSBIN) $(DESTDIR) 2>/dev/null ; \
		echo "done." ; \
	fi

$(CONSPRO): $(CONSOBJ)
	$(CC) $(CFLAGS) -o $@ $+ $(LFLAGS) $(OFLAGS)

%.o: src/%.c src/%.h
	$(CC) $(CFLAGS) -c $<

%.o: src/%.c
	$(CC) $(CFLAGS) -DPROGVERS=\"$(APPVERS)\" -c $<

%.o: ../my1codelib/src/%.c ../my1codelib/src/%.h
	$(CC) $(CFLAGS) -c $<

clean:
	-$(DELETE) $(CONSBIN) *.exe *.o
