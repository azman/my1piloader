/*----------------------------------------------------------------------------*/
#include "my1uart.h"
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define PROGNAME "my1piloader"
/*----------------------------------------------------------------------------*/
#define ERROR_GENERAL -1
#define ERROR_PARAM_PORT -2
#define ERROR_PARAM_BAUD -3
#define ERROR_ABORT -4
#define ERROR_FOPEN -5
/*----------------------------------------------------------------------------*/
#define XMODEM_HEAD_SIZE 3
#define XMODEM_PACK_SIZE 128
#define XMODEM_CSUM_SIZE 1
#define XMODEM_BUFF_SIZE (XMODEM_HEAD_SIZE+XMODEM_PACK_SIZE+XMODEM_CSUM_SIZE)
/*----------------------------------------------------------------------------*/
#define ASCII_SOH 0x01
#define ASCII_ACK 0x06
#define ASCII_NAK 0x15
#define ASCII_EOT 0x04
/*----------------------------------------------------------------------------*/
void about(void) {
	printf("Use: %s [options]\n",PROGNAME);
	printf("Options are:\n");
	printf("  --file <name>   : name of the code file to be loaded.\n");
	printf("  --port <number> : port number between 1-%d.\n",MAX_COM_PORT);
#ifndef DO_MINGW
	printf("  --tty <device>  : alternate device name (useful in Linux).\n");
#endif
	printf("  --stay : keep connection once code uploaded.\n");
	printf("  --help : show this message. overrides other options.\n");
	printf("  --scan : scan and list ports. overrides other options.\n\n");
}
/*----------------------------------------------------------------------------*/
void print_portscan(my1uart_t* port) {
	int test, size = 0;
	printf("--------------------\n");
	printf("COM Port Scan Result\n");
	printf("--------------------\n");
	for (test=1;test<=MAX_COM_PORT;test++) {
		if (uart_prep(port,test)) {
			printf("%s: Ready\n",port->temp);
			size++;
		}
	}
	printf("\nDetected Port(s): %d\n\n",size);
}
/*----------------------------------------------------------------------------*/
int wait_NAK(my1uart_t* port) {
	my1key_t key;
	while (1) {
		key = get_keyhit();
		if (key==KEY_ESCAPE) {
			printf("\nUser abort!\n\n");
			return 0;
		}
		if(uart_incoming(port)&&uart_read_byte(port)==ASCII_NAK)
			break;
	}
	return ASCII_NAK;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1uart_t port;
	my1uart_conf_t conf;
	my1key_t key;
	int term = 1, scan = 0, test, loop, stay = 0;
	char filename[] = "kernel.img", *ptty = 0x0, *psrc = 0x0;
	FILE* pfile;
	/* for xmodem transfer */
	int count = 1, index, check, error = 0;
	unsigned char buff[XMODEM_BUFF_SIZE];
	unsigned long fsize,fcurr;
	/* unbuffered stdout */
	setbuf(stdout,0);
	/* print tool info */
	printf("\n%s - R-Pi Code Loader %s ",PROGNAME,PROGVERS);
	printf("(by azman@my1matrix.org)\n\n");
	/* check program arguments */
	if (argc>1) {
		for (loop=1;loop<argc;loop++) {
			if (argv[loop][0]=='-') {
				/* options! */
				if (!strcmp(argv[loop],"--file")) {
					if (!(psrc=get_param_str(argc,argv,&loop))) {
						printf("** Error getting code file name!\n");
						continue;
					}
				}
				else if (!strcmp(argv[loop],"--port")) {
					if (get_param_int(argc,argv,&loop,&test)<0) {
						printf("** Cannot get port number!\n\n");
						return ERROR_PARAM_PORT;
					}
					else if (test>MAX_COM_PORT) {
						printf("** Invalid port number! (%d)\n\n", test);
						return ERROR_PARAM_PORT;
					}
					term = test;
				}
				else if (!strcmp(argv[loop],"--tty")) {
					if (!(ptty=get_param_str(argc,argv,&loop))) {
						printf("** Error getting tty name!\n");
						continue;
					}
				}
				else if (!strcmp(argv[loop],"--help")) {
					about();
					return 0;
				}
				else if (!strcmp(argv[loop],"--scan")) scan = 1;
				else if (!strcmp(argv[loop],"--stay")) stay = 1;
				else printf("** Unknown option '%s'!\n",argv[loop]);
			}
			else {
				/* not an option */
				if (!psrc) { /* assume file name */
					psrc = argv[loop];
				}
				else {
					printf("** Unknown parameter %s!\n\n",argv[loop]);
					return ERROR_GENERAL;
				}
			}
		}
	}
	/* initialize port */
	uart_init(&port);
#ifndef DO_MINGW
	sprintf(port.name,"/dev/ttyUSB"); /* default on linux? */
#endif
	/* check user requested name change */
	if (ptty) sprintf(port.name,"%s",ptty);
	/* check if user requested a port scan */
	if (scan) {
		print_portscan(&port);
		return 0;
	}
	/* assign default filename if none given */
	if (!psrc) psrc = filename;
	/* try to open file now... abort if cannot open one */
	pfile = fopen(psrc,"rb");
	if (!pfile) {
		printf("** Cannot open file \'%s\'!\n\n",psrc);
		return ERROR_FOPEN;
	}
	/* get file size */
	fseek(pfile,0L,SEEK_END);
	fsize = ftell(pfile);
	fseek(pfile,0L,SEEK_SET);
	fcurr = 0;
	/* try to prepare port with requested terminal */
	if (!term) term = uart_find(&port,0x0);
	if (!uart_prep(&port,term)) {
		about();
		print_portscan(&port);
		printf("** Cannot prepare port '%s%d'!\n\n",port.name,COM_PORT(term));
		return ERROR_GENERAL;
	}
	/* apply specific app baudrate */
	uart_get_config(&port,&conf);
	conf.baud = MY1BAUD115200;
	uart_set_config(&port,&conf);
	/* try opening port */
	if (!uart_open(&port)) {
		printf("** Cannot open port '%s'!\n\n",port.temp);
		return ERROR_GENERAL;
	}
	/* clear input buffer */
	uart_purge(&port);
	/* status report */
	printf("-- Looking for Rasberry Pi board on '%s'... ",port.temp);
	/* wait for signal from board - sync with a NAK! */
	if (wait_NAK(&port)==ASCII_NAK) {
		printf("found!\n\nSending file '%s'...   0%%",psrc);
		/* start sending file */
		while (1) {
			/* pack data */
			buff[0] = ASCII_SOH;
			buff[1] = count;
			buff[2] = 0xFF - count;
			check = buff[0] + buff[1] + buff[2];
			index = 0; loop = 3;
			while (index<XMODEM_PACK_SIZE) {
				test = fgetc(pfile);
				if (feof(pfile)) break;
				buff[loop] = test & 0xFF;
				check += buff[loop];
				index++; loop++;
			}
			/* zero pad final packet */
			while (loop<XMODEM_BUFF_SIZE-1) {
				buff[loop] = 0;
				check += buff[loop];
				loop++;
			}
			/* insert crc byte */
			buff[XMODEM_BUFF_SIZE-1] = check & 0xFF;
			/* update send info */
			fcurr += index;
			if (fcurr>fsize) {
				printf("\n** File error?!(%lu/%lu)\n\n",fcurr,fsize);
				error = 1;
				break;
			}
			/* send data */
			for (loop=0;loop<XMODEM_BUFF_SIZE;loop++)
				uart_send_byte(&port,buff[loop]);
			/* must get ack - else, an error? */
			while (!uart_incoming(&port));
			test = uart_read_byte(&port);
			if (test!=ASCII_ACK) {
				printf("\n** Send error?!(%lu/%lu)[%02x]\n\n",
					fcurr,fsize,test);
				error = 1;
				break;
			}
			printf("\b\b\b\b%3d%%",(int)(fcurr*100/fsize));
			/* check if last packet? */
			if (index<XMODEM_PACK_SIZE||feof(pfile)) {
				/* send EOT */
				uart_send_byte(&port,ASCII_EOT);
				/* must get ack - else, an error? */
				while (!uart_incoming(&port));
				if (uart_read_byte(&port)!=ASCII_ACK) {
					printf("\nEOT error!\n\n");
					error = 1;
				}
				break;
			}
			count++;
			if (count>255) count = 1;
		}
		if (!error)
			printf("\b\b\b\bdone!\n\n## Code should be running...\n\n");
	}
	fclose(pfile);
	if (stay) {
		printf("\nMaintaining port connection.\n");
		while (1) {
			key = get_keyhit();
			if (key==KEY_F10) break;
			if (uart_incoming(&port))
				putchar((byte08_t)uart_read_byte(&port));
			/* check port status? */
			if (!uart_okstat(&port)) {
				printf("** Port Error (%d)! Aborting!\n",port.stat);
				break;
			}
		}
	}
	/* close port */
	uart_done(&port);
	return 0;
}
/*----------------------------------------------------------------------------*/
